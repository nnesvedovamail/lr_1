package com.company;

import java.util.Scanner;

public class calc2 extends calc1 {
    public calc2() {
    }

    public double nod(Scanner sc, double num1, double num2) {
        boolean isDefault = num1 == 0 | num2 == 0;
        if (isDefault) {
            try {
                System.out.print("\nВведите 1 число\n");
                num1 = sc.nextDouble();
                System.out.print("\nВведите 2 число\n");
                num2 = sc.nextDouble();
            } catch (Exception e) {
                System.out.println("Ошибка");
            }
        }


        while (num1 != 0 && num2 != 0) {
            if (num1 > num2) {
                num1 = num1 % num2;
            } else {
                num2 = num2 % num1;
            }
        }
        if (isDefault)
            System.out.println(num1  + num2);
        return num1 + num2;
    }

    public double nok(Scanner sc, double num1, double num2) {

        try {
            System.out.print("\nВведите 1 число\n");
            num1 = sc.nextDouble();
            System.out.print("\nВведите 2 число\n");
            num2 = sc.nextDouble();
        } catch (Exception e) {
            System.out.println("Ошибка");
        }

        double result = 0;
        result = num1 * num2 / nod(sc, num1, num2);
        System.out.println(result);
        return (result);
    }


    public double anotheroperation(Scanner sc, char a) {
        double result;
        calc1 calc = new calc1();
        double num1 = 0, num2 = 0;


        try {

            System.out.print("\nВведите 1 число 1\n");
            num1 = sc.nextDouble();
            System.out.print("\nВведите 2 число 2\n");
            num2 = sc.nextDouble();
        } catch (Exception e) {

            System.out.println("Ошибка");

        }
        switch (a) {
            case '+': {
                result = calc.getadd(num1, num2);
                System.out.print("Результат\n");
                System.out.print(result);
                break;
            }

            case '-': {
                result = calc.getsubctract(num1, num2);
                System.out.print("Результат\n");
                System.out.print(result);
                break;
            }
            case '*': {
                result = calc.getmultiplay(num1, num2);
                System.out.print("Результат\n");
                System.out.print(result);
                break;
            }
            case '/': {
                result = calc.getdivide(num1, num2);
                System.out.print("Результат\n");
                System.out.print(result);
                break;
            }
            default:
                System.out.print("Ошибка\n");

        }
        return -1;
    }
}
