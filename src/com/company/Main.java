package com.company;
import java.util.Scanner;
import java.math.*;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double num1 = 0;
        double num2 = 0;

        while (true) {

            System.out.print("\nВвыберите вид калькулятора:\n ");
            System.out.print("1 Простой калькулятор\n ");
            System.out.print("2 Инженерный калькулятор\n ");
            System.out.print("3 Закончить работу\n");


            int y;
            y = sc.nextInt();

            switch (y) {
                case 1: {
                    System.out.print("Введите операцию: + , - , * , / \n");

                    char a = sc.next().charAt(0);
                    calc2 abr = new calc2();
                    abr.anotheroperation(sc, a);
                    break;
                }
                case 2: {
                    System.out.print("Введите операцию: + , - , * , /, nod(n), nok(k)  \n");
                    char a = sc.next().charAt(0);
                    calc2 c = new calc2();
                    if (a == 'n') {
                        c.nod(sc, num1, num2);
                    } else if ( a == 'k') {
                        c.nok(sc, num1, num2);
                    } else
                        c.anotheroperation(sc, a);
                    break;
                }

                case 3: {
                    System.out.print("Работа завершена\n");
                    System.exit(0);
                }
                default:
                    System.out.print("Такого варианта нет\n");
                    System.exit(0);
            }
        }
    }
}

